#include <iostream>
#include <algorithm>
#include <vector>
#include <ctime>

std::vector<int> ReadVector(size_t max_count) {
    std::vector<int> result;
    result.reserve(max_count);
    for (size_t i = 0; i < max_count; ++i) {
        int elem;
        if (!(std::cin >> elem)) {
            break;
        }
        result.push_back(elem);
    }
    return result;
}

int main() {
    const size_t max_count = 300000u;
    auto data = ReadVector(max_count);
    auto start_time = std::clock();

    std::sort(data.begin(), data.end());

    auto end_time = std::clock();
    double spent = static_cast<double>(end_time - start_time) / CLOCKS_PER_SEC;

    if (!std::is_sorted(data.begin(), data.end())) {
        std::cout << "mmmm\n";
        return 1;
    }

    if (spent > 2.0) {
        std::cout << "this sort sucks\n";
        return 1;
    }
    return 0;
}
