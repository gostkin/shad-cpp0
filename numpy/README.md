# numpy

В этом задании вам нужно реализовать модуль для питона, ускоряющий
алгоритм поиска квантилей.

Модуль должен экспортировать функцию `quantize`. Функция принимает
один `numpy` массив _x_ типа `float` или `double` (без копипасты кода), и параметр _n_ - число
квантилей.

Функция возвращает 2 `numpy`-массива.

  - `boundaries` - _n_ найденных квантилей.
  - `quantized` - массив целых чисел, по одному на каждый элемент
    входного массива. `quantized[i] = sum(x[i] < boundaries)`. Иными
    словами `quantized[i]` показывает, в какой интервал попало число
    `x[i]`.
  
Реализация функции должена быть аналогична функции `quantize_slow` из
файла `test_quantize.py`.

## Установка зависимостей, сборка и тестирование

0. В задании нужно реализовать модуль для питона, поэтому первым делом
   нужно поставить третий питон :)

1. Вам понадобятся 2 модуля: `numpy` и `pytest`.
   Самый простой способ - установить из их _pip_:
     - `sudo pip3 install pytest numpy`

2. Cгенерируйте `cmake` проект, и убедитесь что модуль собрался.
   В директории сборки должен появиться файл `quantize.so`.

3. Запустите прогон тестов. `env PYTHONPATH=<path to cmake build directory> pytest`.

   * `env PYTHONPATH=` устанавливает путь, по которому интерпретатор
     питона найдёт модуль.

   * Тесты должны упасть с ошибкой импорта, потому что модуль еще не
     рализован.
   
   * Запустить отдельный тест можно с помощью флага `-k`, например:
   `env PYTHONPATH=build/release pytest -k edge_cases`

   * Чтобы запустить тесты под gdb, используйте команду
   `env PYTHONPATH=build/release gdb --args python3 -m pytest`

## Python C-API и Numpy C-API

Основная часть этого задания состоит в том, чтобы разобраться в двух API.

 - `Python.h` - API для взаимодействия с интерпретатором
   питона. Прочитайте
   [tutorial](https://docs.python.org/3/extending/extending.html), для
   ознакомления. Ответы на вопросы ищите
   в [reference](https://docs.python.org/3/c-api/index.html).

 - Интересные функции из `Python.h`:
   * `PyArg_ParseTuple`
   * `PyErr_SetString`
   * `PyTuple_Pack`
 - Интересные переменные из `Python.h`:
   * `PyExc_TypeError`
   * `PyExc_RuntimeError`

 - `numpy/arrayobject.h` - API для взаимодействия с библиотекой
   numpy. Работа с этим API описана в
   [документации](https://docs.scipy.org/doc/numpy-1.13.0/user/c-info.html)

 - Интересные функции из `numpy/arrayobject.h`
   * `import_array()`
   * `PyArray_Check`
   * `PyArray_Size`
   * `PyArray_TYPE`
   * `PyArray_DATA`
   * `PyArray_SimpleNew`
