#include <catch.hpp>

#include <cmath>
#include <string>
#include <optional>

#include <camera_options.h>
#include <render_options.h>
#include <commons.hpp>
#include <raytracer.h>

void CheckImage(const std::string& obj_filename, const std::string& result_filename,
                const CameraOptions& camera_options, const RenderOptions& render_options,
                std::optional<std::string> output_filename = std::nullopt) {
    auto image = Render(kBasePath + "tests/" + obj_filename, camera_options, render_options);
    if (output_filename.has_value()) {
        image.Write(output_filename.value());
    }
    Image ok_image(kBasePath + "tests/" + result_filename);
    Compare(image, ok_image);
}

TEST_CASE("Texture_Cube", "[raytracer]") {
    CameraOptions camera_opts(640, 480);
    camera_opts.look_from = std::array<double, 3>{0.9, 0.7, 0.7};
    camera_opts.look_to = std::array<double, 3>{0.0, 0.0, 0.0};
    RenderOptions render_opts{1};
    CheckImage("tex_cube/cube.obj", "tex_cube/result.png", camera_opts, render_opts);
}

TEST_CASE("Texture_Box", "[raytracer]") {
    CameraOptions camera_opts(800, 600);
    camera_opts.look_from = std::array<double, 3>{1.5, 1.5, -0.1};
    camera_opts.look_to = std::array<double, 3>{1, 1.2, -2.8};
    RenderOptions render_opts{8};
    CheckImage("tex_box/scene.obj", "tex_box/result.png", camera_opts, render_opts);
}

TEST_CASE("Mirror_Texture", "[raytracer]") {
    CameraOptions camera_opts(800, 600);
    camera_opts.look_from = std::array<double, 3>{2, 1.5, -0.1};
    camera_opts.look_to = std::array<double, 3>{1, 1.2, -2.8};
    RenderOptions render_opts{8};
    CheckImage("mirror_tex/scene.obj", "mirror_tex/result.png", camera_opts, render_opts);
}

/* TEST_CASE("Head", "[raytracer]") {
    CameraOptions camera_opts(800, 600);
    camera_opts.look_from = std::array<double, 3>{-0.022567, 0.028160, 0.209246};
    camera_opts.look_to = std::array<double, 3>{0.07301561, -0.21657955, -0.20323739};
    RenderOptions render_opts{1};
    CheckImage("head/head.OBJ", "head/result.png", camera_opts, render_opts,
               "../raytracer-b1/tests/head/result.png");
} */